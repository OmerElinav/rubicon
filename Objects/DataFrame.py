from functools import partial

from pyspark.sql import DataFrame


class Rubicon(DataFrame):
    def __init__(self, df, loaded_funcs=None):
        self.df = df
        self.loaded_funcs = loaded_funcs \
            if loaded_funcs is not None \
            else dict()
        for name, func in self.loaded_funcs.items():
            self._load(name, func, stateful=False)

    def __getattribute__(self, item) -> DataFrame:
        if hasattr(DataFrame, item):
            return self._rubicon_decoration(DataFrame.__getattribute__(self.df, item))
        return DataFrame.__getattribute__(self, item)

    def _use_state(self, f):
        return partial(f, self.df)

    def _rubicon_decoration(self, f):
        if not callable(f):
            return f

        def wrapper(*args, **kwargs):
            result = f(*args, **kwargs)
            return Rubicon(result, self.loaded_funcs) \
                if isinstance(result, DataFrame) \
                else result

        return wrapper

    def load(self, *callables):
        for callable_ in callables:
            name = _func_name(callable_)
            self._load(name, callable_)

    def _load_from_cache(self, *callables):
        for name, callable_ in callables:
            self._load(name, callable_)

    def _load(self, name, callable_, stateful=True):
        func = self._use_state(callable_) if stateful else callable_
        self.loaded_funcs[name] = func
        setattr(self, name, func)


def _func_name(callable_):
    try:
        name = callable_.__name__
    except AttributeError:
        name = callable_.__class__.__name__
    return name
