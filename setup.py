import setuptools

setuptools.setup(
    name="rubicon",
    version="0.0.1",
    author="Omer Elinav",
    author_email="omerelinav@gmail.com",
    description="a minimalistic wrapper for pyspark",
    url="https://gitlab.com/OmerElinav/rubicon",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)